// File:	rpthread.c

// List all group member's name: Daniel Kim, Rani Sayed
// username of iLab: 
// iLab Server:	plastic.cs.rutgers.edu

//all things defined in case multiple files include rpthread.h

//comparator and finder defined before the default definition
#define COMPARATOR(X, Y, MIN_HEAP)	(MIN_HEAP[X]->prio < MIN_HEAP[Y]->prio)
#define FINDER(PQ, X, VAL)	(PQ->min_heap[X]->tid == VAL)

#include "rpthread.h"
// INITAILIZE ALL YOUR VARIABLES HERE
// YOUR CODE HERE

//to make it easier to tell if scheduler was already invoked
//so we dont accidentally create more and just context swap after first time
static int init_d;
static rpthread_t id_counter = 1;
static long total_scheduled;
static int empty_mlfq;
static uint m_id_counter = 1;

//timers
static struct itimerval arm_timeval;	//use to arm timer
static struct itimerval disarm_timeval;	//user to disarm timer
static struct sigaction sa;


#define QUANTUM_SEC 0
#define QUANTUM_USEC 5000

//could probably move most of these to pqueue.h
//call the ones without _ in the name. Ignore item_type and item_mem
//PQ = priority queue pointer
//ITEM = tcb to put in (no pointer)
//VAR = tcb var to have the tcb 'returned' to
#define ITEM_TYPE		tcb*
#define ITEM_MEM(PQ)	PQ->min_heap[i]->prio
#define INIT(PQ)		_INIT(PQ, ITEM_TYPE)
#define PUSH(PQ, ITEM)	_PUSH(PQ, ITEM, ITEM_TYPE)
#define POP(PQ, VAR)	_POP(PQ, VAR, ITEM_TYPE)	//Declare var then feed to macro. Same with peek
#define FIND(PQ, VAL, VAR)	_FIND(PQ, FINDER, VAL, VAR)
#define PRINT_QUEUE(PQ) _PRINT_QUEUE(PQ, ITEM_MEM(PQ))
//PEEK is simple enough so no redefine

//some globals structures we might need
static tcb* curr_tcb;	//running tcb
static ucontext_t scheduler_ctx;
PQUEUE(ITEM_TYPE);		//define pqueue type and declare struct
static b_list* join_list;

//thread states
enum{
	READY = 0,
	#define READY READY
	BLOCKED,
	#define BLOCKED BLOCKED
	RUNNING,
	#define RUNNING RUNNING
	DONE
	#define DONE DONE
};

//idk i like to modularize for no reason
static void timer_handler(int signum);
static void disarm_timer();
static void arm_timer();
static void schedule();
#ifndef MLFQ
static void sched_stcf();
static pqueue* pq;		
#else
static void sched_mlfq();
static void raise_priority();
static void pick_queue();
static pqueue** mlfq;
#endif
static void init_scheduler();


/* create a new thread */
int rpthread_create(rpthread_t * thread, pthread_attr_t * attr, 
                      void *(*function)(void*), void * arg) {

	//disarm timer if initialized
	int time = 0;
	if(init_d != 0){
		time = getitimer(ITIMER_PROF, &arm_timeval);
		//setitimer(ITIMER_PROF, &disarm_timer, &arm_timer);
	}

	if(init_d == 0){
		init_scheduler();
	}

	//main ctx, new ctx
	ucontext_t* nctx = (ucontext_t*) malloc(sizeof(ucontext_t));
	
	if(getcontext(nctx) < 0){
		perror("Could not get context for new context\n");
		return -1;
	}

	//modify new ctx
	nctx->uc_stack.ss_sp = malloc(SIGSTKSZ);
	if(nctx->uc_stack.ss_sp == NULL){
		perror("Malloc for context stack failed.\n");
		return -1;
	}
	nctx->uc_stack.ss_size = SIGSTKSZ;
	nctx->uc_stack.ss_flags = 0;
	nctx->uc_link = &scheduler_ctx;

	makecontext(nctx, (void*)function, 1, arg);

	//make tcb for new ctx
	tcb* thread_t = (tcb*) malloc(sizeof(tcb));
	thread_t->tid = id_counter++;
	*thread = thread_t->tid;
	thread_t->status = RUNNING;
	thread_t->ctx = nctx;
	thread_t->prio = 0;

	//push new thread
	#ifndef MLFQ
	PUSH(pq, thread_t)	
	#else
	thread_t->level = 0;
	PUSH(mlfq[0], thread_t)
	#endif
	
	if(init_d == 0){	//first call of create
		//set to one since first call done after
		init_d = 1;
		
		//set main context
		tcb* main_tcb = (tcb*) malloc(sizeof(tcb));
		main_tcb->tid = 0;
		main_tcb->status = RUNNING;
		
		ucontext_t* mctx = (ucontext_t*) malloc(sizeof(ucontext_t));
		getcontext(mctx);
		main_tcb->ctx = mctx;

		curr_tcb = main_tcb;
		swapcontext(curr_tcb->ctx, &scheduler_ctx);
	}else{
		curr_tcb->prio += time;
		//swap every time since scheduler will start up again even after the queue is empty and only main is running.
		swapcontext(curr_tcb->ctx, &scheduler_ctx);
	}
	
	return 0;
};

/* give CPU possession to other user-level threads voluntarily */
int rpthread_yield() {
	
	//disarm timer while going to scheduler
	int time = getitimer(ITIMER_PROF, &arm_timeval);
	disarm_timer();
	
	curr_tcb->status = READY;
	curr_tcb->prio += time;
	swapcontext(curr_tcb->ctx, &scheduler_ctx);
	return 0;
};
		
/* terminate a thread */
void rpthread_exit(void *value_ptr) {
	// Deallocated any dynamic memory created when starting this thread

	//disarm timer while going to scheduler
	disarm_timer();

	curr_tcb->return_value = value_ptr;
	curr_tcb->status = DONE;
	
	b_list* temp_node = (b_list*) malloc(sizeof(b_list));
	temp_node->curr = curr_tcb;
	temp_node->next = join_list;
	join_list = temp_node;

	//free(curr_tcb->ctx->uc_stack.ss_sp);
	free(curr_tcb->ctx);
	return;
};


/* Wait for thread termination */
int rpthread_join(rpthread_t thread, void **value_ptr) {

	curr_tcb->status = BLOCKED;
	
	tcb* join_t = NULL;

	//wait until tcb to join is found
	while(1){		
		//search join list
		b_list* prev = NULL;
		b_list* temp_list = join_list;
		b_list* post;
		while(temp_list != NULL){
			if(temp_list->curr->tid == thread){
				join_t = temp_list->curr;
				break;
			}
			prev = temp_list;
			post = ((temp_list->next) != NULL) ? temp_list->next->next:NULL;
			temp_list = temp_list->next;
		}
		//join
		if(join_t != NULL){
			if(value_ptr != NULL && join_t->return_value != NULL) *value_ptr = join_t->return_value;
			curr_tcb->status = RUNNING;
			if(prev == NULL) join_list == NULL;
			else prev->next = post;
			free(join_t);
			break;
		}
	}
	return 0;
};


/* initialize the mutex lock */
int rpthread_mutex_init(rpthread_mutex_t *mutex, 
                          const pthread_mutexattr_t *mutexattr) {
	//Initialize data structures for this mutex
	(*mutex).owner = NULL;
	(*mutex).m_id = m_id_counter;
	
	(*mutex).list = (b_list*) malloc(sizeof(b_list));
	(*mutex).size = 0;
	m_id_counter++;
	return 0;
};

/* aquire the mutex lock */
int rpthread_mutex_lock(rpthread_mutex_t *mutex) {
		
		if((*mutex).owner != NULL){
			curr_tcb -> status = BLOCKED;
			if((*mutex).size == 0){
				if((*mutex).list == NULL){
					(*mutex).list = (b_list*) malloc(sizeof(b_list));
				}
				(*mutex).list->curr = curr_tcb;
				(*mutex).size++;
			} else {
				b_list* _next = (*mutex).list;
				(*mutex).list = malloc(sizeof(b_list));
				(*mutex).list-> curr = curr_tcb;
				(*mutex).list-> next = _next;
				(*mutex).size++;
			}
			curr_tcb->prio += getitimer(ITIMER_PROF, &arm_timeval);
			disarm_timer();
			swapcontext(curr_tcb->ctx, &scheduler_ctx);
		}else{
			(*mutex).owner = curr_tcb;
		}
        return 0;
};

/* release the mutex lock */
int rpthread_mutex_unlock(rpthread_mutex_t *mutex) {
	disarm_timer();	
	(*mutex).owner  = NULL;
	if((*mutex).list != NULL && (*mutex).list->curr != NULL){
		(*mutex).list->curr->status = READY;
		b_list* head = (*mutex).list;
		(*mutex).list = (*mutex).list->next;
		free(head);
		(*mutex).size--;
	}
	arm_timer();
	return 0;
};

/* destroy the mutex */
int rpthread_mutex_destroy(rpthread_mutex_t *mutex) {
	// Deallocate dynamic memory created in rpthread_mutex_init
	while((*mutex).list != NULL){
		b_list* temp = ((*mutex).list -> next);
		free((*mutex).list);
		(*mutex).list = temp;
	}
	return 0;
};

static void init_scheduler(){
	
	if(getcontext(&(scheduler_ctx)) == -1){
		perror("Could not get context for scheduler\n");
		return;
	}
	//modify new ctx
	scheduler_ctx.uc_stack.ss_sp = malloc(SIGSTKSZ);
	if(scheduler_ctx.uc_stack.ss_sp == NULL){
		perror("Malloc for context stack failed.\n");
		return;
	}
	scheduler_ctx.uc_stack.ss_size = SIGSTKSZ;
	scheduler_ctx.uc_stack.ss_flags = 0;
	makecontext(&scheduler_ctx, (void*)&schedule, 0);
	
	//setup timers
	arm_timeval.it_value.tv_usec = QUANTUM_USEC;
	arm_timeval.it_value.tv_sec = QUANTUM_SEC;
	disarm_timeval.it_value.tv_usec = 0;
	disarm_timeval.it_value.tv_sec = 0;
		
	//setup signal handler;
	memset(&sa, 0, sizeof(sa));
	sa.sa_handler = &timer_handler;
	sigaction(SIGPROF, &sa, NULL);
	
	#ifndef MLFQ
	INIT(pq);
	#else
	mlfq = (pqueue**) malloc(sizeof(pqueue*)*4);
	int i;
	for(i=0; i<4; i++){
		INIT(mlfq[i])
	}
	#endif

	join_list = NULL;

}

/* scheduler */
static void schedule() {
	
	//setup scheduler ctx. should be included in every ctx uc_link
	// schedule policy

		#ifndef MLFQ
		sched_stcf();
		//printf("Swapped to %d\n", curr_tcb->tid);
		arm_timer();
		setcontext(curr_tcb->ctx);
		#else 
		// Choose MLFQ
		sched_mlfq();
		//printf("Swapped to %d\n", curr_tcb->tid);
		arm_timer();
		setcontext(curr_tcb->ctx);
		#endif

}

#ifndef MLFQ
/* Preemptive SJF (STCF) scheduling algorithm */
static void sched_stcf() {
	if(pq->curr_size == 0){
		disarm_timer();	
	}else if(curr_tcb->status != DONE){
		tcb* temp_tcb = curr_tcb;	//swap current context
		POP(pq, curr_tcb)		
		PUSH(pq, temp_tcb)		
		if(curr_tcb->status == READY) curr_tcb->status = RUNNING;	
	}else{	
		POP(pq, curr_tcb)
		if(curr_tcb->status == READY) curr_tcb->status = RUNNING;	
	}
}
#else
/* Preemptive MLFQ scheduling algorithm */
static void sched_mlfq() {

	total_scheduled++;
	if(empty_mlfq == 1){
		disarm_timer();
	}else if(curr_tcb->status != DONE){
		//relinquishes CPU
		if(curr_tcb->status == READY){
			tcb* temp_tcb = curr_tcb;
			pick_queue();
			
			//update when it was last scheduled
			curr_tcb->last_scheduled = total_scheduled;
			raise_priority();	//raise priorities
	
			//put back in same queue level
			PUSH(mlfq[temp_tcb->level], temp_tcb)

		//if uses entire quantum	
		}else{
			tcb* temp_tcb = curr_tcb;
			pick_queue();
			
			//update when it was last scheduled
			curr_tcb->last_scheduled = total_scheduled;
			//raise priorities of thread's that haven't run in a while
			raise_priority();

			//lower thread's priority
			if(temp_tcb->level < 3) temp_tcb->level += 1;
			PUSH(mlfq[temp_tcb->level], temp_tcb)
		}
		if(curr_tcb->status == READY) curr_tcb->status == RUNNING;
	}else{
		pick_queue();
		if(curr_tcb->status == READY) curr_tcb->status = RUNNING;	
		curr_tcb->last_scheduled = total_scheduled;
		raise_priority();
	}
}

//raise priorities at each level if beyond last-scheduled threshold
//only done for threads at the top of each queue.
static void raise_priority(){
	tcb* temp_tcb = NULL;
	int curr_level;
	for(curr_level = 1; curr_level <= 3; curr_level++){
		
		PEEK(mlfq[curr_level], temp_tcb)
		if(temp_tcb == NULL) continue;
		//check top of pqueue (PEEK) and check last scheduled
		if(total_scheduled - temp_tcb->last_scheduled > 5){	
			//raise priority
			temp_tcb->last_scheduled = total_scheduled;
			POP(mlfq[curr_level], temp_tcb)
			if(temp_tcb->level != 0) temp_tcb->level -= 1;
			PUSH(mlfq[curr_level], temp_tcb)
		}
	}
}

//pick queue to pop from
static void pick_queue(){
	int level = 0;
	for(; level < 4; level++){
		if(mlfq[level]->curr_size > 0){
			POP(mlfq[level], curr_tcb)
			break;
		}
	}
	if(level == 4){
		empty_mlfq = 1;
	}
}
#endif

static void arm_timer(){
	arm_timeval.it_value.tv_usec = QUANTUM_USEC;
	arm_timeval.it_value.tv_sec = QUANTUM_SEC;
	setitimer(ITIMER_PROF, &arm_timeval, NULL);
}

static void disarm_timer(){
	setitimer(ITIMER_PROF, &disarm_timeval, NULL);
}

static void timer_handler(int signum){
	curr_tcb->prio += QUANTUM_USEC+QUANTUM_SEC;
	swapcontext(curr_tcb->ctx, &scheduler_ctx);
}

