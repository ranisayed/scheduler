#include <stdio.h>
#include <unistd.h>
#include <pthread.h>
#include "../rpthread.h"

/* A scratch program template on which to call and
 * test rpthread library functions as you implement
 * them.
 *
 * You can modify and use this program as much as possible.
 * This will not be graded.
 */
long* a;
rpthread_mutex_t mtx;
void func2(void* args){
	long i;
rpthread_mutex_lock(&mtx);
	for(i=0; i<1000000000; i++){		
		*a++;
	}
rpthread_mutex_unlock(&mtx);
	rpthread_exit(&i);

}

void func(void* args){
	
	int num;
	rpthread_create(&num, NULL, &func2, NULL);
	void** val;
	int num2;
	int val2;
	rpthread_create(&num2, NULL, &func2, NULL);
	rpthread_join(num2, NULL);
	rpthread_join(num, NULL);
	rpthread_exit(&val2);
}

int main(int argc, char **argv) {

	/* Implement HERE */
	rpthread_mutex_init(&mtx, NULL);
	a = (long*) malloc(sizeof(long));
	int num, num2, num3, num4, num5, num6;
	
	rpthread_create(&num, NULL, &func, NULL);
	rpthread_create(&num2, NULL, &func, NULL);
	rpthread_create(&num3, NULL, &func, NULL);
	rpthread_create(&num4, NULL, &func, NULL);
	rpthread_create(&num5, NULL, &func, NULL);
	rpthread_join(num, NULL);
	rpthread_join(num2, NULL);
	rpthread_join(num3, NULL);
	rpthread_join(num4, NULL);
	rpthread_join(num5, NULL);
	
	rpthread_mutex_destroy(&mtx);
	int i;
	for(i=0; i<100000000; i++){
		
	}

	rpthread_create(&num6, NULL, &func, NULL);
	void** val2;
	rpthread_join(num6, NULL);
	
	printf("ended\n");
	return 0;
}
