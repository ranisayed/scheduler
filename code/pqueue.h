#ifndef _pqueue_h
#define _pqueue_h

#ifndef COMPARATOR
#define COMPARATOR(X, Y, MIN_HEAP) (MIN_HEAP[X] < MIN_HEAP[Y])
#define FINDER(PQ, X, VAL)	(PQ->min_heap[X] == VAL)
#endif

#define PQUEUE(ITEM_TYPE)	\
	typedef struct pqueue{		\
		long total_size;	\
		long curr_size;		\
		ITEM_TYPE* min_heap;		\
	}pqueue;

#define FIND_PARENT(CURR_INDEX, PARENT_INDEX)		\
	do{								\
	if(CURR_INDEX%2 == 0) PARENT_INDEX = (CURR_INDEX-2)/2;	\
	else PARENT_INDEX = (CURR_INDEX-1)/2;	\
	}while(0); 

#define SWAP(MIN_HEAP, TO_SWAP, SWAPPED, ITEM_TYPE)	\
	do{		\
	ITEM_TYPE temp = MIN_HEAP[SWAPPED];	\
	MIN_HEAP[SWAPPED] = MIN_HEAP[TO_SWAP];	\
	MIN_HEAP[TO_SWAP] = temp;	\
	}while(0);

#define SIFT_UP(MIN_HEAP, CURR_INDEX, ITEM_TYPE)		\
	do{								\
	long sift_up_index = CURR_INDEX;	\
	while(sift_up_index != 0){			\
		long parent_index;			\
		FIND_PARENT(sift_up_index, parent_index);	\
		\
		if(COMPARATOR(sift_up_index, parent_index, MIN_HEAP)){				\
			SWAP(MIN_HEAP, sift_up_index, parent_index, ITEM_TYPE)	\
			sift_up_index = parent_index;	\
		}else{	\
			break;	\
		}	\
	}	\
	}while(0);	

#define SIFT_DOWN(MIN_HEAP, CURR_INDEX, CURR_SIZE, ITEM_TYPE)		\
	do{		\
	int left = (2*CURR_INDEX)+1;	\
	int right = (2*CURR_INDEX)+2;	\
	int sift_down_temp = CURR_INDEX;	\
	while(left < CURR_SIZE){		\
		if(COMPARATOR(left, right, MIN_HEAP)){				\
			SWAP(MIN_HEAP, sift_down_temp, left, ITEM_TYPE);	\
			sift_down_temp = left;		\
		}else{						\
			SWAP(MIN_HEAP, sift_down_temp, right, ITEM_TYPE);	\
			sift_down_temp = right;		\
		}	\
		left = (2*sift_down_temp)+1;	\
		right = (2*sift_down_temp)+2;	\
	}		\
	}while(0);

#define _INIT(PQ, ITEM_TYPE)	\
	do{						\
	PQ = (pqueue*) malloc(sizeof(pqueue));	\
	PQ->total_size = 10;	\
	PQ->curr_size = 0;		\
	PQ->min_heap = (ITEM_TYPE*) malloc(sizeof(ITEM_TYPE)*(PQ->total_size));		\
	}while(0);

#define _PUSH(PQ, ITEM, ITEM_TYPE)	\
	do{								\
	if(PQ->curr_size == PQ->total_size){	\
		long old_size = PQ->total_size;		\
		PQ->min_heap = (ITEM_TYPE*) realloc(PQ->min_heap, sizeof(ITEM_TYPE)*(old_size*2));	\
		PQ->total_size = old_size*2;		\
	}										\
											\
	PQ->min_heap[PQ->curr_size] = ITEM;		\
	PQ->curr_size += 1;						\
											\
	SIFT_UP(PQ->min_heap, PQ->curr_size-1, ITEM_TYPE)			\
	}while(0);					

#define _POP(PQ, VAR, ITEM_TYPE)	\
	do{	\
	VAR = PQ->min_heap[0];	\
	\
	PQ->min_heap[0] = PQ->min_heap[PQ->curr_size-1];	\
	PQ->curr_size -= 1;	\
	SIFT_DOWN(PQ->min_heap, 0, PQ->curr_size, ITEM_TYPE)	\
	}while(0);

#define PEEK(PQ, VAR)	\
	do{	\
	if(PQ->curr_size != 0){	\
		VAR = PQ->min_heap[0];	\
	}else{	\
		VAR = NULL;	\
	}	\
	}while(0);

#define _FIND(PQ, FINDER, VAL, VAR)	\
	do{	\
		int pqueue_i = 0;	\
		while(pqueue_i < PQ->curr_size){		\
			if(FINDER(PQ, pqueue_i, VAL)){	\
				VAR = PQ->min_heap[pqueue_i];	\
				break;	\
			}	\
			pqueue_i++;	\
		}	\
	}while(0);

#define _PRINT_QUEUE(PQ, ITEM_MEM)		\
	do{		\
	int i=0;	\
	for(; i<PQ->curr_size; i++){	\
		printf("%u | ", ITEM_MEM);	\
	}	\
	printf("\n");	\
	}while(0);

#endif
